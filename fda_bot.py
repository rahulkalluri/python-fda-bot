# IF ON WORK COMPUTER
# set HTTP_PROXY=http://proxy.osumc.edu:8080
# set HTTPS_PROXY=https://proxy.osumc.edu:8080

import requests
import json

proxies = {
    'http': 'http://proxy.osumc.edu:8080',
    'https': 'https://proxy.osumc.edu:8080',
}

drug_events = requests.get('https://api.fda.gov/drug/event.json?limit=1', proxies=proxies)

print(drug_events)
